from django.shortcuts import render, get_object_or_404
from .models import Post,Tag
from .forms import PostForm
from django.utils import timezone
from django.shortcuts import redirect

# Create your views here.


def post_list(request):
    posts = Post.objects.all()
    return render(request, 'blog/post_list.html', {'posts': posts})


def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})


def post_new(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            title=form.cleaned_data['title']
            text=form.cleaned_data['text']
            post=Post(title=title,text=text)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            tags=form.cleaned_data['tags'].split(',')
            for tag in tags:
                tag_name=Tag.objects.filter(tag_name=tag).first()
                if tag_name is  None:
                    tag_name=Tag(tag_name=tag)
                    tag_name.save()
                tag_name.posts.add(post)   
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})


def post_edit(request, pk):
    post=get_object_or_404(Post,pk=pk)
    if request.method=='POST':
        form=PostForm(request.POST,instance=post)
        if form.is_valid():
            post=form.save(commit=False)
            post.author=request.user
            post.published_date=timezone.now()
            post.save()
            return redirect('post_detail',pk=post.pk)
    else:
        form=PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})
