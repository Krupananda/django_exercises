from .models import Post
from django import forms

class PostForm(forms.Form):
    title=forms.CharField(max_length=200)
    text=forms.CharField(widget=forms.Textarea)
    tags=forms.CharField(max_length=300)
